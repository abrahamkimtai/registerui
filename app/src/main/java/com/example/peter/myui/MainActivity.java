package com.example.peter.myui;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextInputEditText Username;
    private TextInputEditText Useremail;
    private CardView Cardreg;
    private CardView Cardexit;
    private RadioGroup Group;
    private String gender;
    private RadioButton btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //cast
        Username = (TextInputEditText) findViewById(R.id.username);
        Useremail = (TextInputEditText) findViewById(R.id.useremail);
        Cardreg = (CardView) findViewById(R.id.cardreg);
        Cardexit= (CardView) findViewById(R.id.cardexit);
        Group = (RadioGroup) findViewById(R.id.radioGroup);
        gender = "";



        Cardexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Cardreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!TextUtils.isEmpty(Username.getText().toString()) && !TextUtils.isEmpty(Useremail.getText().toString())){
                    if (Group.getCheckedRadioButtonId()!=-1){
                        Intent newActivity = new Intent(MainActivity.this,page.class);
                        startActivity(newActivity);
                        finish();

                    }
                    else{
                        Toast.makeText(MainActivity.this,"Select gender",Toast.LENGTH_LONG).show();
                    }

                }
                else{
                    Toast.makeText(MainActivity.this,"Empty fields detected of gender not selected",Toast.LENGTH_LONG).show();
                }

            }
        });
    }



}
